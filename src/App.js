import React, {useState} from 'react';
import AddTaskForm from "./AddTaskForm";
import Task from "./Task";
import './App.css';

const App = () => {

    const [tasks, setTasks] = useState([]);
    const [text, setText] = useState('');

    const onChangeHandler = (event) => {
        setText(event.target.value)
    }

    const addTaskHandler = (event) => {
        event.preventDefault();
        const tasksCopy = [...tasks];

        const newTask = {
            text: text,
            id: new Date().toISOString()
        };

        tasksCopy.push(newTask);
        setTasks(tasksCopy);
    };

    const removeTask = index => {
        const tasksCopy = [...tasks];
        tasksCopy.splice(index, 1);
        setTasks(tasksCopy);
    }

    return (
        <div className="App">
            <AddTaskForm
                text={text}
                onSubmitSend={addTaskHandler}
                onChangeText={onChangeHandler}
            />
            <Task
                tasks={tasks}
                remove={removeTask}
            />
        </div>
    );
};

export default App;
