import React from 'react';

const AddTaskForm = props => {
    return (
        <form onSubmit={props.onSubmitSend}>
            <input id="main-input" type="text" value={props.text} onChange={(event) => props.onChangeText(event)}/>
            <button className="btn-add" type='submit'>Add</button>
        </form>
    );
};

export default AddTaskForm;