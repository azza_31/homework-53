import React from 'react';

const Task = props => {
    return (
        props.tasks.map((task, index) => {
            return (
                <div className='view' key={index}>
                    <p>{task.text}</p>
                    <button onClick={() => props.remove(index)}>X</button>
                </div>
            )
        })
    )
};

export default Task;